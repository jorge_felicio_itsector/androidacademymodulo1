package com.example.scorekepper;

import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;


import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    int scoreTeamA=0;
    int scoreTeamB=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void increaseTeamATwoPoints(View view){
        scoreTeamA = scoreTeamA+2;
        displayTeamA(scoreTeamA);
    }

    public void increaseTeamAThreePoints(View view){
        scoreTeamA = scoreTeamA+3;
        displayTeamA(scoreTeamA);
    }

    public void increaseTeamASixPoints(View view){
        scoreTeamA = scoreTeamA+6;
        displayTeamA(scoreTeamA);
    }

    public void increaseTeamBTwoPoints(View view){
        scoreTeamB = scoreTeamB+2;
        displayTeamB(scoreTeamB);
    }

    public void increaseTeamBThreePoints(View view){
        scoreTeamB = scoreTeamB+3;
        displayTeamB(scoreTeamB);
    }

    public void increaseTeamBSixPoints(View view){
        scoreTeamB = scoreTeamB+6;
        displayTeamB(scoreTeamB);
    }

    public void resetScore(View view){
        scoreTeamA = 0;
        scoreTeamB = 0;
        displayTeamA(scoreTeamA);
        displayTeamB(scoreTeamB);
    }

    private void displayTeamA(int score){
        TextView scoreA = (TextView) findViewById(R.id.scoreTeamA);
        scoreA.setText(""+score);

    }

    private void displayTeamB(int score){
        TextView scoreB = (TextView) findViewById(R.id.scoreTeamB);
        scoreB.setText(""+score);

    }
}